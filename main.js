/* Задание 12 - Создать интерфейс StarWars DB для данных из SWAPI.
*
* Используя SWAPI, вывести информацию по всех планетам с пагинацией и возможностью просмотреть доп.
* информацию в модальном окне с дозагрузкой смежных ресурсов из каждой сущности.
*
* Данные для отображения в карточке планеты:
    * 1. Наименование (name)
* 2. Диаметр (diameter)
* 3. Население (population)
* 4. Уровень гравитации (gravity)
* 5. Природные зоны (terrain)
* 6. Климатические зоны (climate)
*
* При клике по карточке отображаем в модальном окне всю информацию
* из карточки, а также дополнительную:
    * 1. Список фильмов (films)
* - Номер эпизода (episode_id)
* - Название (title)
* - Дата выхода (release_date)
* 2. Список персонажей
* - Имя (name)
* - Пол (gender)
* - День рождения (birth_year)
* - Наименование родного мира (homeworld -> name)
*
* Доп. требования к интерфейсу:
    * 1. Выводим 10 карточек на 1 странице
* 2. Пагинация позволяет переключаться между страницами, выводить общее количество страниц и текущие выбранные
* элементы в формате 1-10/60 для 1 страницы или 11-20/60 для второй и т.д.
* 3. Используем Bootstrap для создания интерфейсов.
* 4. Добавить кнопку "Показать все" - по клику загрузит все страницы с планетами и выведет
* информацию о них в един (опцианально)
*/


(async () => {
    function getPlanetsData(pageNumber = 1) {
        return fetch(`https://swapi.dev/api/planets/?page=${pageNumber}`)
            .then(response => response.json())
            .then(({ count: total, results: planets }) => ({ total, planets }));
    }

    function renderPlanetCards(planets) {
        const cardsHtml = planets.map(
            planet => `
        <div class="card">
          <div class="card-body">
            <h5 class="card-title">${planet.name}</h5>
            <ul>
              ${
                ['diameter', 'population', 'gravity', 'terrain', 'climate'].map(
                    key => `
                    <li>${key}: ${planet[key]}</li>
                  `
                ).join('')
            }
            </ul>
            <button data-url="${planet.url}" type="button" class="btn btn-primary open-modal-js" data-bs-toggle="modal" data-bs-target="#exampleModal">
            Подробнее
            </button>
          </div>
        </div>
      `
        ).join('');

        document.querySelector('.cards').innerHTML = cardsHtml;
    }

    function getPlanet(url){
        return fetch(url)
            .then(response=>response.json())
    }

    function renderPaginator(total, pageSize = 10) {
        let items = '';
        for (let i = 0; i < total / pageSize; i++) {
            items += `<li class="page-item"><a class="page-link" href="#">${i + 1}</a></li>`;
        }

        let listHtml = `
      <ul class="pagination">
        <li class="page-item disabled"><a class="page-link" href="#">Previous</a></li>
        ${items}
        <li class="page-item"><a class="page-link" href="#">Next</a></li>
      </ul>
    `;

        document.querySelector('.js-paginator').innerHTML = listHtml;
        document.querySelectorAll('.page-item')[1].classList.add('active')
    }

    function managePagination() {
        document.querySelector('.js-paginator').addEventListener(
            'click',
            async event => {
                event.preventDefault();
                const paginatorValue = document.querySelectorAll('.page-item');
                if (!event.target.classList.contains('page-link')) {
                    return;
                }
                let pageNumber = event.target.textContent;
                const activeEl=document.querySelector('.active');
                //НАЖАТИЕ КНОПКИ NEXT
                if (pageNumber=='Next'){
                    pageNumber=Number(activeEl.textContent);
                    const planetsData = await getPlanetsData(pageNumber+1);
                    paginatorValue.forEach(item=>item.classList.remove('disabled','active'));
                    paginatorValue[pageNumber+1].classList.add('active');
                    renderPlanetCards(planetsData.planets)
                    if(pageNumber==paginatorValue.length-3){
                        paginatorValue[paginatorValue.length-1].classList.add('disabled')
                    }
                }
                else if (pageNumber=='Previous'){
                    pageNumber=Number(activeEl.textContent);
                    const planetsData = await getPlanetsData(pageNumber-1);
                    paginatorValue.forEach(item=>item.classList.remove('disabled','active'));
                    paginatorValue[pageNumber-1].classList.add('active');
                    renderPlanetCards(planetsData.planets)
                    console.log(pageNumber)
                    if(pageNumber==2){
                        document.querySelector('.page-item').classList.add('disabled')
                    }
                }
                else {
                    const planetsData = await getPlanetsData(pageNumber);
                    renderPlanetCards(planetsData.planets);
                    paginatorValue.forEach(item=>item.classList.remove('disabled','active'));
                    event.target.parentNode.classList.add('active');
                }
                if (pageNumber==1){
                    document.querySelector('.page-item').classList.add('disabled')
                }
                if(pageNumber==paginatorValue.length-2){
                    paginatorValue[paginatorValue.length-1].classList.add('disabled')
                }
            }
        );
    }
    function manageModalData(){
        document.querySelector('.cards').addEventListener('click',
            async event => {
                if(!event.target.classList.contains('open-modal-js')){
                    return
                }
                const url = event.target.getAttribute('data-url');

                const {name,films,residents} = await getPlanet(url);
                const people = await Promise.all(
                    residents.map(
                        residentsUrl=>fetch(residentsUrl).then(response=>response.json())
                    )
                );
                const filmsData = await Promise.all(
                    films.map(
                        filmsUrl=>fetch(filmsUrl).then(response=>response.json())
                    )
                );
                const modalBodyHtml = `
        <h3>Список Фильмов</h3>
          <ul>
              ${
                    filmsData.map(
                        filmsData => `<li>Номер эпизода: ${filmsData.episode_id}</li><li>Название: ${filmsData.title}</li><li>Дата выхода: ${filmsData.release_date}</li><br>`
                    ).join('')
                }
          </ul>
        <h3>Персонажи</h3>
          <ul>
            ${
                    people.map(
                        person => `<li>Имя: ${person.name}</li><li>Пол: ${person.gender}</li><li>День рождения: ${person.birth_year}</li><br>`
                    ).join('')
                }
          </ul>
      `;
                document.querySelector('.modalBody').innerHTML=modalBodyHtml;
                document.querySelector('.js-planet-title').textContent = `${name}`
            }
        )
    }

    const planetsData = await getPlanetsData();
    renderPlanetCards(planetsData.planets);
    renderPaginator(planetsData.total);
    managePagination();
    manageModalData();
})();
